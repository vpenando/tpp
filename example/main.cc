#include <iostream> // std::cout, std::hex
#include <math.hh>  // POW, FACTORIAL, IS_PRIME

using namespace tpp::math;

int main(){
  std::cout << "Is pow(fac(3) + fac(3), 2) prime?" << std::endl;
  constexpr auto fac3 = FACTORIAL<3>;
  constexpr auto pow_fac3 = POW<fac3 * 2, 2>;
  constexpr auto prime = IS_PRIME<pow_fac3>;
  std::cout << std::hex << prime;
}
