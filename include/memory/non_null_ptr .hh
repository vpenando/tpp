#ifndef __NON_NULL_PTR_HH__
#define __NON_NULL_PTR_HH__

#include <memory>  // std::unique_ptr, std::nullptr_t, std::default_delete
#include <utility> // std::make_unique

namespace tpp {
  namespace memory {

    template<class T,
      class DeleterType = std::default_delete<T>,
      class PointerType = std::unique_ptr<T, DeleterType>,
      class NullType    = std::nullptr_t
    >
    class non_null_ptr : private PointerType {
    public:
      template<class ...Args>
      explicit non_null_ptr(Args&&... args) : PointerType(std::make_unique<T>(args...)) {}
      non_null_ptr(NullType)            = delete;
      non_null_ptr& operator=(NullType) = delete;

      using PointerType::PointerType;
      using PointerType::get;
      using PointerType::operator bool;
      using PointerType::operator*;
      using PointerType::operator->;
    };

  } // namespace tpp::memory
} // namespace tpp

#endif // __NON_NULL_PTR_HH__
