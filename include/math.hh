#ifndef __TPP_MATH_HH__
#define __TPP_MATH_HH__

#include <math/factorial.hh> // tpp::math::factorial
#include <math/pow.hh>       // tpp::math::pow
#include <math/prime.hh>     // tpp::math::is_prime

namespace tpp {
  namespace math {
    
    template<unsigned N>
    constexpr auto FACTORIAL = factorial<N>::value;

    template<int X, unsigned N>
    constexpr auto POW = pow<X, N>::value;

    template<unsigned N>
    constexpr auto IS_PRIME = is_prime<N>::value;
    
    template<class T>
    constexpr auto PI = static_cast<T>(3.14159265358979323846);
    
  } // namespace tpp::math
} // namespace tpp

#endif // __TPP_MATH_HH__
