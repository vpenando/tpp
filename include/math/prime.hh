#ifndef __TPP_MATH_PRIME_HH__
#define __TPP_MATH_PRIME_HH__

#include <type_traits> // std::integral_constant

namespace tpp {
  namespace math {

    namespace detail {
      template<unsigned N, unsigned Divisor>
      struct is_prime_impl
        : std::integral_constant<bool, N % Divisor != 0 && is_prime_impl<N, Divisor - 1>::value> {};

      template<unsigned N>
      struct is_prime_impl<N, 1> : std::true_type {};

      template<unsigned N>
      struct is_prime_impl<N, 0> : std::true_type {};
    } // namespace tpp::math::detail

      //! \type is_prime
      //! \brief Defines a static member "value" as true if "N" is prime number, false otherwise
      //! \param N The number we work on
    template<unsigned N>
    struct is_prime : detail::is_prime_impl<N, N - 1> {};

    template<> struct is_prime<0> : std::true_type {};
    template<> struct is_prime<1> : std::true_type {};

  } // namespace tpp::math
} // namespace tpp

#endif // __TPP_MATH_PRIME_HH__
