#ifndef __TPP_MATH_FACTORIAL_HH__
#define __TPP_MATH_FACTORIAL_HH__

#include <type_traits> // std::integral_constant

namespace tpp {
  namespace math {

    //! \type  factorial
    //! \brief Returns the factorial of a specified template parameter
    //! \param N (Template param) The number we work on
    template<unsigned N>
    struct factorial : std::integral_constant<decltype(N), N * factorial<N - 1u>::value> {};

    // Specialization for N == 0
    template<>
    struct factorial<0u> : std::integral_constant<unsigned, 1u> {};

  } // namespace tpp::math
} // namespace tpp

#endif // __TPP_MATH_FACTORIAL_HH__
