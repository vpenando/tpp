#ifndef __TPP_MATH_POW_HH__
#define __TPP_MATH_POW_HH__

#include <type_traits> // std::integral_constant

namespace tpp {
  namespace math {

    //! \type  pow
    //! \brief Returns X^N
    //! \param X (Template param) Base
    //! \param N (Template param) Exponent
    template<int X, unsigned N>
    struct pow : std::integral_constant<decltype(X), X * pow<X, N - 1u>::value> {};

    template<int X>
    struct pow<X, 0u> : std::integral_constant<decltype(X), 1> {};

  } // namespace tpp::math
} // namespace tpp

#endif // __TPP_MATH_POW_HH__
