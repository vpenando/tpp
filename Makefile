TARGET=tpp_test
CXX=clang++-3.6
CXXFLAGS = -std=c++14 -Wall -Werror -pedantic -Wold-style-cast -Woverloaded-virtual -Wfloat-equal -Wwrite-strings \
			-Wpointer-arith -Wconversion -Wshadow -Wredundant-decls -Winit-self -Wswitch-default -Wundef \
			-Winline -Wunused -Wuninitialized
INCLUDE=./include
OBJ=main.o

main.o: example/main.cc
	$(CXX) $(CXXFLAGS) -I$(INCLUDE) -c $<
	
all: $(OBJ)
	$(CXX) -o $(TARGET) $(OBJ) $(CXXFLAGS) -I$(INCLUDE)
	
clean:
	rm -rf *.o
	rm $(TARGET)